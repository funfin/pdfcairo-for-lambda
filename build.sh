#!/bin/bash

: "${S3BUCKET:?Variable not set or empty}"

sudo yum update -y
sudo yum -y groupinstall "Development Tools"
sudo yum -y install cairo cairo-devel

mkdir -p ~/tmp/{usr,etc,var,libs,install,downloads,tar,dist}

wget -P ~/tmp/downloads \
         http://downloads.sourceforge.net/freetype/freetype-2.6.1.tar.bz2 \
         http://www.freedesktop.org/software/fontconfig/release/fontconfig-2.11.1.tar.bz2 \
         http://xmlsoft.org/sources/libxml2-2.9.2.tar.gz \
         http://poppler.freedesktop.org/poppler-0.37.0.tar.xz \
         https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs926/ghostscript-9.26.tar.gz \
&& ls ~/tmp/downloads/*.tar.* | xargs -i tar xf {} -C ~/tmp/libs/

pushd .

####################################
cd ~/tmp/libs/freetype*
sed -e "/AUX.*.gxvalid/s@^# @@" \
    -e "/AUX.*.otvalid/s@^# @@" \
    -i modules.cfg              &&

sed -e 's:.*\(#.*SUBPIXEL.*\) .*:\1:' \
    -i include/freetype/config/ftoption.h  &&

./configure --prefix=/home/ec2-user/tmp/usr --disable-static &&
make
make install

####################################
cd ~/tmp/libs/libxml*
PKG_CONFIG_PATH=~/tmp/usr/lib/pkgconfig/:$PKG_CONFIG_PATH \
./configure --prefix=/home/ec2-user/tmp/usr --disable-static --with-history &&
make
make install

####################################
cd ~/tmp/libs/fontconfig*
export FONTCONFIG_PKG=`pwd`

PKG_CONFIG_PATH=~/tmp/usr/lib/pkgconfig/:$PKG_CONFIG_PATH \
./configure --prefix=/home/ec2-user/tmp/usr        \
            --sysconfdir=/home/ec2-user/tmp/etc    \
            --localstatedir=/var \
            --disable-docs       \
            --enable-libxml2 &&
make
make install

####################################
cd ~/tmp/libs/ghostscript*
#PKG_CONFIG_PATH=~/tmp/usr/lib/pkgconfig/:$PKG_CONFIG_PATH \
./configure --prefix=/home/ec2-user/tmp/usr --disable-static --with-history &&
make
make install


####################################
cd ~/tmp/libs/poppler*
PKG_CONFIG_PATH=~/tmp/usr/lib/pkgconfig/:$FONTCONFIG_PKG:$PKG_CONFIG_PATH \
./configure --prefix=/opt      \
            --sysconfdir=/opt/etc           \
            --enable-build-type=release \
            --enable-cmyk               \
            --enable-xpdf-headers && make

make install DESTDIR="/home/ec2-user/tmp/install"

unset FONTCONFIG_PKG
popd

cp -r tmp/install/opt/* tmp/dist/
cp -r tmp/usr/bin/ tmp/dist/
cp -r tmp/usr/include/ tmp/dist/
cp -r tmp/usr/lib/ tmp/dist/
cp -r tmp/usr/share/ tmp/dist/

DIST_FILE=pdftocairo-lambda-layer.zip

push .
cd ~/tmp/dist
zip -r ~/tmp/$DIST_FILE .
popd

aws s3 cp ~/tmp/${DIST_FILE} s3://"${S3BUCKET}"/${DIST_FILE}

aws lambda publish-layer-version \
    --layer-name pdfcairo-layer \
    --description "pdfcairo layer" \
    --content S3Bucket="${S3BUCKET}",S3Key=${DIST_FILE} \
    --compatible-runtimes java8 python2.7 python3.6 python3.7
